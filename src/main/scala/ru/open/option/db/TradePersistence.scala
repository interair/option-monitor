package ru.open.option.db

;

import java.util.concurrent.ConcurrentHashMap

import akka.actor.ActorSystem
import ru.open.option.model.Trade

object TradePersistence {

  def apply()(implicit system: ActorSystem) = new TradePersistence()

  val trades = new ConcurrentHashMap[Int, Trade]()
}

class TradePersistence(implicit val system: ActorSystem) {

  import system.dispatcher



  def getTrades[T](id: Option[Int] = None): Seq[Trade] = {
    return null;
  }

  def updateTrade(id: Int, widget: Trade): Boolean = {
  }

  def createTrade[T](widget: Trade): Option[Trade] = {
  }

  def deleteTrade(id: Int): Boolean = {

  }
}
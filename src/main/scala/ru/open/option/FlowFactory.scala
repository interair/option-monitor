package ru.open.option

import ru.open.option.amqp.RabbitRegistry

import scala.concurrent.ExecutionContext

import akka.stream.scaladsl.Flow
import akka.util.ByteString

import com.typesafe.scalalogging.slf4j.LazyLogging

import io.scalac.amqp.{Delivery, Message, Routed}

import RabbitRegistry._


/**
 * This is our flow factory.
 * 
 */
trait FlowFactory extends LazyLogging {
  
  /** Flow responsible for mapping the incoming RabbitMQ message to our domain input. */
  def consumerMapping: Flow[Delivery, ByteString] =
    Flow[Delivery].map(_.message.body)
  
  /** Flow performing our domain processing. */
  def domainProcessing(implicit ex: ExecutionContext): Flow[ByteString, FortsMessage] =
    Flow[ByteString].
  
    // to string
    map { _.utf8String }.
    
    // do something time consuming
    mapAsync { DomainService.expensiveCall }.

    // classify message
    map { DomainService.classify }
    
  /** Flow responsible for mapping the domain processing result into a RabbitMQ message. */
  def publisherMapping: Flow[FortsMessage, Routed] =
    Flow[FortsMessage] map(cMsg => cMsg match {
      case Option(msg) => Routed(routingKey = outOkQueue.name, Message(body = ByteString(cMsg.message)))
      case Futures(msg) => Routed(routingKey = outNokQueue.name, Message(body = ByteString(cMsg.message)))
    })
    
}
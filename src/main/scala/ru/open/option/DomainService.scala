package ru.open.option

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Random

import com.typesafe.scalalogging.slf4j.LazyLogging


sealed trait FortsMessage {
  def message: String
}
case class Option(message: String) extends FortsMessage
case class Futures(message: String) extends FortsMessage

object DomainService extends LazyLogging {
  
  /** Classify message */
  def classify(msg: String): FortsMessage = {

    val processedMessage = msg + " [message processed]"
   
//    if () {
      logger.debug("message classified as 'threat'")
      Futures(processedMessage)
//    } else {
      logger.debug("message classified as 'safe'")
      Option(processedMessage)
//    }
  }
  
  /** Do something time consuming - like go to sleep. */
  def expensiveCall(msg: String)(implicit ec: ExecutionContext): Future[String] = Future {
    val millis = Random.nextInt(2000) + 1000
    logger.debug(s"message: '$msg' \n will be held for $millis ms")
    Thread.sleep(millis)
    msg
  }
}
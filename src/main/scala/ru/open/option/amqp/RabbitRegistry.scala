package ru.open.option.amqp

import io.scalac.amqp.{Direct, Exchange, Queue}


object RabbitRegistry {

  val inboundExchange = Exchange("forts.inbound.exchange", Direct, true)
  val inboundQueue = Queue("forts.inbound.queue")
    
  val outboundExchange = Exchange("forts.outbound.exchange", Direct, true)
  val outOkQueue = Queue("forts.ok.queue")
  val outNokQueue = Queue("forts.nok.queue")
}

package ru.open.option.rest

import akka.actor.{ActorRefFactory, ActorSystem}
import com.github.vonnagy.service.container.http.routing.RoutedEndpoints
import ru.open.option.model.Trade
import spray.http.MediaTypes._
import spray.http._
import spray.httpx.unmarshalling._

class RestApi(implicit val system: ActorSystem,
                    actorRefFactory: ActorRefFactory) extends RoutedEndpoints with TradeHandler {

  // Import the default Json marshaller and un-marshaller
  implicit val marshaller = jsonMarshaller
  implicit val unmarshaller = jsonUnmarshaller[Trade]

  val route = {
    pathPrefix("trades") {
      get {
        // GET /trades
        pathEnd {
          respondWithMediaType(`application/json`) {
            // Push the handling to another context so that we don't block
            getTrade()
          }
        } ~
          // GET /trades/{id}
          path(IntNumber) { id =>
            respondWithMediaType(`application/json`) {
              // Push the handling to another context so that we don't block
              getTrade(Some(id))
            }
          }
      } ~
        // POST /trades
        post {
          // Simulate the creation of a widget. This call is handled in-line and not through the per-request handler.
          entity(as[Trade]) { widget =>
            respondWithMediaType(`application/json`) {
              // Push the handling to another context so that we don't block
              createTrade(widget)
            }
          }
        } ~
        // PUT /widgets/{id}
        put {
          path(IntNumber) { id =>
            // Simulate the update of a product. This call is handled in-line and not through the per-request handler.
            entity(as[Trade]) { widget =>
              // Push the handling to another context so that we don't block
              updateTrade(id, widget)
            }
          }
        } ~
        // DELETE /widgets/{id}
        delete {
          path(IntNumber) { id =>
            // Delete the widget
            deleteTrade(id)
          }
        }

    }
  }
}
package ru.open.option.rest

import akka.actor.ActorSystem
import ru.open.option.db.TradePersistence
import ru.open.option.model.Trade
import spray.http.StatusCodes._
import spray.httpx.marshalling.{Marshaller, ToResponseMarshaller}
import spray.routing._

trait TradeHandler {

  implicit val system: ActorSystem
  val persistor = TradePersistence()

  def getTrade[T](id: Option[Int] = None)(implicit marshaller: Marshaller[T]): Route = ctx => {
    id match {
      case Some(widgetId) =>
        // Return a specific widget
        val data = persistor.getTrades(id)
        if (data.size > 0) {
          implicit def x = marshaller.asInstanceOf[Marshaller[Trade]]
          ctx.complete(data.head)
        } else {
          ctx.reject(NotFoundRejection("The widget could not be located"))
        }

      case _ =>
        // Return all widgets
        implicit def x = marshaller.asInstanceOf[Marshaller[Iterable[widget]]]
        ctx.complete(persistor.getTrades(None))
    }
  }

  def updateTrade(id: Int, trade: Trade): Route = ctx => {
    // Update a specific widget
    persistor.updateTrade(id, trade) match {
      case true =>
        ctx.complete(NoContent)
      case false =>
        ctx.reject(NotFoundRejection("The widget could not be located"))
    }
  }

  def createTrade[T](trade: Trade)(implicit marshaller: Marshaller[Trade]): Route = ctx => {
    // Create a specific widget
    persistor.createTrade(trade) match {
      case Some(w) =>
        ctx.complete(w)(ToResponseMarshaller.fromMarshaller(Created))
      case None =>
        ctx.reject(DuplicateRejection("The widget could not be located"))
    }
  }

  def deleteTrade(id: Int): Route = ctx => {
    // Delete a specific widget
    persistor.deleteTrade(id) match {
      case true =>
        ctx.complete(NoContent)
      case false =>
        ctx.reject(NotFoundRejection("The widget could not be located"))
    }
  }

}
name := """option-monitor"""

version := "2.0"

scalaVersion := "2.11.5"

resolvers ++= Seq(
  "Scalaz Bintray Repo" at "https://dl.bintray.com/scalaz/releases",
  "snapshots"           at "http://oss.sonatype.org/content/repositories/snapshots",
  "releases"            at "http://oss.sonatype.org/content/repositories/releases",
  "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
  "Scalaz Bintray Repo" at "https://dl.bintray.com/scalaz/releases",
  "spray repo"          at "http://repo.spray.io"
)


scalacOptions ++= Seq(
  "-unchecked",
  "-deprecation",
  "-Xlint",
  "-Ywarn-dead-code",
  "-language:_",
  "-target:jvm-1.8",
  "-encoding", "UTF-8"
)

crossPaths := false

parallelExecution in Test := false


libraryDependencies ++= {
  val containerVersion = "1.0.2"
  val configVersion = "1.3.0"
  val akkaVersion = "2.3.11"
  val liftVersion = "2.6.2"
  val sprayVersion = "1.3.3"
  val specsVersion = "3.6.1"
  Seq(
    "com.github.vonnagy"  %% "service-container"                    % containerVersion,
    "com.github.vonnagy"  %% "service-container-metrics-reporting"  % containerVersion,
    "com.typesafe.akka"          %%  "akka-actor"               % akkaVersion,
    "com.typesafe.akka"          %%  "akka-stream-experimental" % "0.10",
    "io.scalac"                  %%  "reactive-rabbit"          % "0.2.1",
    "com.typesafe.scala-logging" %%  "scala-logging-slf4j"      % "2.1.2",
    "ch.qos.logback"             %   "logback-core"             % "1.1.2",
    "ch.qos.logback"             %   "logback-classic"          % "1.1.2",
    "io.spray"                   %% "spray-can"                 % sprayVersion,
    "io.spray"                   %% "spray-routing"             % sprayVersion,
    "net.liftweb"                %% "lift-json"                 % liftVersion
  )
}
